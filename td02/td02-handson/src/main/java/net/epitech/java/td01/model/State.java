package net.epitech.java.td01.model;

public enum State
{
	FREE,
	BUSY;
}